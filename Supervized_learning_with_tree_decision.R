# Author Marion Estoup
# E-mail : marion_110@hotmail.fr
# February 2021

############# Supervized learning 
## Decision tree, ROC curve and cross validation 
## Objectives : use binary tree decision then validate models with supervized learning, cross validation and ROC curve

# Get to know the classifiers 
# Binary tree decision 

# The package rpart has functions to build binary tree decision
### 1 - Install and Load package rpart and rpart.plot

install.packages('rpart')
library('rpart')

install.packages('rpart.plot')
library('rpart.plot')


### 2 - Data Exploration
# Loading the data 
data(kyphosis) 

# Data dimensions
dim(kyphosis)
# Gives 81 and 4

kyphosis
# We have kyphosis, age, number and start

# Get to know the data
# Data Class
class(kyphosis) 
# It's a data frame

# Data first rows
head(kyphosis) 

# Data names (variables)
names(kyphosis) 
colnames(kyphosis) 
# We have kyphosis, age, number and start

# The kyphosis variable
head(kyphosis$Kyphosis) # We see two levels (absent or present)
class(kyphosis$Kyphosis) 
# It's a factor

# The age variable
head(kyphosis$Age) 
class(kyphosis$Age)
# It's an integer


### 3 - Build a tree on the data to explain the kyphosis variable
tree = rpart(Kyphosis ~ Age + Number+Start,data=kyphosis) 
print(tree)
rpart.plot(tree)
text(tree, use.n=TRUE)
post(tree) # create a postscript file 
# The variables are discretized automatically thanks to the rpart package


### 4 - Another tree 
tree = rpart(Kyphosis ~ Age + Number+Start, data=kyphosis, control=rpart.control(minsplit=10) )
rpart.plot(tree,compress=T) 
text(tree, use.n=TRUE)

# The difference between the two previous trees / What does minsplit do ? :
# The tree construction depends on a lot of parameters. Here we have very few observations in our data 
# The minsplit can decrease the number of nodes in our tree 
# The other argument that we can make vary is cp which controls the complexity 
# We can use rpart(Kyphosis ~ .,data=kyphosis) to build a tree with all variables 
# Use the previous tree to predict the class (presence or absence of kyphosis) of new cases with new features

# Minsplit controls the pre trimming compared to the learning data. For example 10 is the number of 
# observations that should exist in a node before choosing another node. The other element that we can make
# vary is the complexity (number of nodes in the tree)



### 5 - Class Prediction vs Class Probability : allows to trace the ROC curve 
nouvellesDonnees = data.frame(Age=c(56,90), 
                              Start=c(15,3),Number=c(3,2),Kyphosis=c("absent","absent")) 
predict(tree,newdata= nouvellesDonnees,type="class") 
 # Do we predict the presence or absence of the disease with these features ? The disease is absent for both child

# Do the same prediction with type="prob" instead of "class"
predict(tree,newdata= nouvellesDonnees,type="prob")


### 6 - Evaluation on the learning data 
# The confusion matrix, the sensitivity and the specificity measures allow to evaluate a classifier
# We can calculate those info 

tree = rpart(Kyphosis ~ Age + Number+Start,data=kyphosis) 
tree
pred = predict(tree,newdata=kyphosis,type="class") 
pred
matriceConfusion = table(kyphosis$Kyphosis,pred)
matriceConfusion

# We can separate the population into two samples 
taille <- floor(0.75 * nrow(kyphosis)) # floor allows to chose randomly the samples
set.seed(123) # sampling function
train_ind <- sample(seq_len(nrow(kyphosis)), size = taille) # indication of the new samples
train <- kyphosis[train_ind, ]
test <- kyphosis[-train_ind, ]

tree = rpart(Kyphosis ~ Age + Number+Start,data=train)
pred = predict(tree,newdata=test,type="class")
matriceConfusion = table(test$Kyphosis,pred)
matriceConfusion

# Realization of a tree 
tree_t = rpart(Kyphosis ~ Age + Number+Start,data=train) 
print(tree_t)

rpart.plot(tree_t)

# The same data is used to learn the tree (rpart) and to test the tree (predict)
# Now we can calculate the accuracy, sensitivity, specificity

TP = matriceConfusion[1,"absent"] # True positive
FP = matriceConfusion[1,"present"] # False positive
FN = matriceConfusion[2,"absent"] # False negative
TN = matriceConfusion[2,"present"] # True negative

accuracy = (TP+TN)/(TP+FN+FP+TN) 
print(accuracy)

sensibilite=TP/(TP+FN) # sensitivity
print(sensibilite)



### The Naive Bayesian classifier
# Load the package e1071
install.packages('e1071')
library('e1071')


classifierNB = naiveBayes(Kyphosis ~ Age + Number+Start,data=kyphosis) 
predict(classifierNB,newdata=kyphosis)
predict(classifierNB,newdata=kyphosis, type="raw")


### 2 - Evaluation of the classifier with ROC curve
library('ROCR')

data(ROCR.simple)
pred <- prediction(ROCR.simple$predictions, ROCR.simple$labels) 
perf <- performance(pred,"tpr","fpr")
plot(perf,colorize=TRUE)

# The prediction corresponds to the probability to be part of the class that we want to predict and that is indicated in the label

### 3 - ROC curve with a cross validation 
data(ROCR.xval)
pred <- prediction(ROCR.xval$predictions, ROCR.xval$labels)
perf <- performance(pred,"tpr","fpr")
plot(perf,col="grey82",lty=3) 
plot(perf,lwd=3,avg="vertical",spread.estimate="boxplot",add=TRUE)

# Here the results of the prediction are in a list with a size 10 where each elements correspond to an iteration of the cross validation








